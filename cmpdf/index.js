/* eslint-env browser, commonjs, es6 */
/* eslint-disable no-console */
"use strict";

var pdfManagerM = require("../src/core/pdf_manager.js");
var pdfJSGlobal = require("../src/shared/global.js");
var prim = require("../src/core/primitives.js");
var workerMod = require("../src/core/worker");
var parser = require("../src/core/parser.js")
pdfJSGlobal.PDFJS.verbosity = -10;

// TODO: dictToBytes()
// writing a dictionary to bytes shouldn't be difficult
// it would simplify things here
// we could simply replace a stream's dictionary with
// an equivalent one instead of manually replacing
// fields in the underlying buffer
// 
// TODO: implement a PDF writer. It will be MUCH easier to work with js objects
// representing PDF and write out semantically equivalent bytes
// instead of directly manipulating bytes
// 
// This will be useful for changing stream dictionaries,
// as well as contentstream/postscript showText() showSpacedText() setFont(), etc

exports.parse = function(pdfBuf){
	return new CMPDF(pdfBuf);
}

function CMPDF(pdfBuf){
	this.buffer = pdfBuf;
	this.stringMetrics = null;
	var hook = {
		reads: [],
		xrefs: [],
		log: function(obj, level){
			if (level) obj.level = level;
			this.reads.push(obj);
		},
		logXref: function(obj){
			this.xrefs.push(obj);
			this.log(obj, 'xref');
		}
	};
	var pdfManager = new pdfManagerM.LocalPdfManager("docId", pdfBuf, null, hook);
	var pdfDocument = pdfManager.pdfDocument;
	this.pdfDocument = pdfDocument;
	pdfDocument.parseStartXRef();
	pdfDocument.parse();
	this.numPages = pdfDocument.numPages;
	pdfDocument.xref.entries.forEach(function(e, i){
		pdfDocument.xref.fetch(new prim.Ref(i, e.gen));
	});
	this.tree = buildTree(hook.reads, hook.xrefs);
	this.pagePromises = [];
	this.pages = [];
}

CMPDF.prototype.getTextContent = function(cb){
	var self = this;
	var promises = new Array(this.numPages);
	for (var i = 0, ii = this.numPages; i < ii; i++){
		promises[i] = this.getPageTextContent(i);
	}
	return Promise.all(promises).then(function(res){
		var stringMetrics = [];
		var prevPageMetrics;
		var prevItemMetrics;
		var pdfText = res.map(function(page, index){
			var pageMetrics = {
				index: index,
				textContent: page,
				items: []
			};
			pageMetrics.start = prevPageMetrics ? prevPageMetrics.end : 0;

			var pageString = page.items.map(function(item){
				var itemMetrics = {
					item: item
				};
				itemMetrics.start = prevItemMetrics ? prevItemMetrics.end : pageMetrics.start;
				var itemString = item.str.slice();
				// TODO: this assumes that each character of the string is 1 byte long
				itemMetrics.end = itemMetrics.start + itemString.length;
				prevItemMetrics = itemMetrics;
				pageMetrics.items.push(itemMetrics);
				return itemString;
			}).join('');

			pageMetrics.end = pageMetrics.start + pageString.length;
			prevPageMetrics = pageMetrics;
			stringMetrics.push(pageMetrics);
			return pageString;
		}).join('');
		self.stringMetrics = stringMetrics;
		self.textContent = pdfText;
		cb(null, pdfText);
	}, function(ex){
		cb(ex);
	});
}

CMPDF.prototype.getPageTextContent = function(pageIndex){
	var self = this;
	var i = pageIndex;
	var pdfDocument = this.pdfDocument;
	var tree = this.tree;

	if (this.pagePromises[pageIndex]) {
		return this.pagePromises[pageIndex].then(getPageTextContent);
	}

	var pagePromise = pdfDocument.getPage(i)
	.then(function(page){
		// var opsList = {
		// 	fnArray: [],
		// 	argsArray: []
		// };
		return Promise.all([
			page,
			page.content,
			page.extractTextContent(new workerMod.WorkerTask('extractTextContent'+i), true, true)//,
			// new Promise(function(resolve, reject){
			// 	page.getOperatorList({
			// 		send: function(op, args){
			// 			if (op != "RenderPageChunk") return; // skip
			// 			opsList.fnArray = opsList.fnArray.concat(args.operatorList.fnArray);
			// 			opsList.argsArray = opsList.argsArray.concat(args.operatorList.argsArray);
			// 		}
			// 	}, new workerMod.WorkerTask('getOperatorList'+i), 'oplist'
			// 	).then(function(){
			// 		resolve(opsList);
			// 	}).catch(reject);
			// })
		]);
	})
	.then(function(results){
		var page = {
			index: i,
			page: results[0],
			textContent: results[2],
			operatorList: results[3]
		};
		var contentStreams = results[1];
		if (!(contentStreams instanceof Array))
			contentStreams = [contentStreams];
		page.contentStreams = contentStreams;
		return page;
	})
	.then(function(page){
		var contentStreams = page.contentStreams;
		contentStreams.forEach(function(contentStream){
			var entry;
			var objId;
			try {
				objId = /(\d+)R(\d+)?/.exec(contentStream.objId || contentStream.dict.objId);
			} catch(e){
				console.error('no objId in content stream', e, e.stack);
			}
			if (objId) {
				objId = objId.slice(1);
				entry = tree.entries[objId[0]][objId[1] || 0];
			}
			else throw new Error("couldn't find this stream in tree");

			var lexer = new parser.Lexer(contentStream);
			lexer.hook = {
				reads: [],
				strings: [],
				log: function(entry){
					this.reads.push(entry);
					if (entry.type.indexOf('string') >= 0)
						this.strings.push(entry);
				}
			};
			var token;
			while(token != parser.EOF){
				try {
					token = lexer.getObj();
				} catch(e){
					// console.error(e);
					lexer.nextChar();
				}
			}
			// sanityCheck(lexer.hook.reads, contentStream);
			entry.children = lexer.hook.reads;
			entry.strings = lexer.hook.strings;
			contentStream.entry = entry;
		})
		return page;
	})
	.then(function(page){
		return self.pages[pageIndex] = page;
	})
	.catch(function(ex){
		console.error(ex, ex.stack);
		throw ex;
	});

	this.pagePromises[pageIndex] = pagePromise;

	return pagePromise.then(getPageTextContent);

	function getPageTextContent(page){
		return page.textContent;
	}
}

CMPDF.prototype.replace = function(changes){
	/*
	each change consists of {start, end, string, [font]}
	changes MUST be sorted by start offset
	 */
	// TODO: what if we'e replacing newlines or adding newlines in the change?
	var stringMetrics = this.stringMetrics;
	if (!stringMetrics) throw new Error("Must call getTextContent() fist");

	var byteChanges = [];

	// TODO: group changes by content stream
	// TODO: current limitation: 1 content stream per page
	var changesPerPage = [];
	changes.forEach(function(change){
		var affectedPages = stringMetrics.filter(affectedItemsFilter.bind(null, change));
		var _changesPerPage = affectedPages.map(changesPerItemMap.bind(null, change));
		_changesPerPage.forEach(function(change, index){
			var page = affectedPages[index];
			addChangeToPage(page, change);
		});
	});
	changesPerPage.forEach((page) => {
		var decodedStreamChanges = [];

		var contentStream = this.pages[page.metrics.index].contentStreams[0];
		contentStream.reset();
		var contentStreamBuffer = contentStream.peekBytes();
		var reads = getReads(contentStream);
		var parserReads = reads.parser;
		parserReads = parserReads.sort(hookEntriesComparator);
		var fontCmds = [];
		parserReads.forEach(function(read, index, arr){
			if (prim.isCmd(read.obj, 'Tf')){
				fontCmds.push({
					pos: read.streamEndPos,
					name: arr[index - 2].obj.name,
					size: arr[index - 1].obj
				})
			}
		});

		function findCurrentFont(pos){
			var prevFont = fontCmds[0];
			var currFont;
			for (var i = 1; i < fontCmds.length; i++){
				currFont = fontCmds[i];
				if (prevFont.pos <= pos && pos < currFont.pos)
					return prevFont
				prevFont = currFont;
			}
			return prevFont;
		}

		page.changes.forEach(function(change){
			// TODO: support case where change string is not the same size as item
			// TODO: support multiple directional text
			
			var affectedItems = page.metrics.items.filter(affectedItemsFilter.bind(null, change));
			var changesPerItem = affectedItems.map(changesPerItemMap.bind(null, change));

			changesPerItem.forEach(function(change, index){
				var item = affectedItems[index];
				var itemString = item.item.str;
				var tags = itemString.tags;
				var argumentReadIndex = findObjectIndexByPos(parserReads, tags[0].taggedString.streamBeginPos);
				var argumentRead = parserReads[argumentReadIndex];

				var commandRead;
				// find next command
				parserReads.slice(argumentReadIndex).some(function(r){
					if (prim.isCmd(r.obj)){
						commandRead = r;
						return true;
					}
				})
				
				var origCommand = commandRead.obj.cmd;
				var newline = origCommand[0] != 'T';

				var currentFont = findCurrentFont(argumentRead.streamBeginPos);

				// TODO: find old font
				var origFontName = currentFont.name;
				var origFontSize = currentFont.size;
				var newFontName = "CMFONT";

				// TODO: adjacent items don't need to re-set font every time
				var firstFontString = "\n\n/" + newFontName + " " + origFontSize + " Tf\n\n";
				var secondFontString = "\n\n/" + origFontName + " " + origFontSize + " Tf\n\n";
				var changeString = "\n(" + change.string + ") " + "Tj" + "\n"

				function injectItem(start, end, newline){
					var showCommand = "Tj"; // j must be lowercase
					if (newline)
						showCommand = "'"; // single quote
					decodedStreamChanges.push({
						start: start,
						end: end,
						bytes: asciiToBytes(
							firstFontString
							+ "\n(" + change.string + ") " + showCommand + "\n"
							+ secondFontString
						) // TODO
					});
				}

				var beginningOfLineByteOffset;
				if (prim.isCmd(commandRead.obj, '"')){
					beginningOfLineByteOffset = parserReads[argumentReadIndex - 2].streamBeginPos;
				} else {
					beginningOfLineByteOffset = argumentRead.streamBeginPos;
				}
				if (change.start == item.start && change.end == item.end){
					// replacing the entire item
					injectItem(beginningOfLineByteOffset, commandRead.streamEndPos, newline);
					return;
				}
				var tag;
				var adjustment = 0;
				var isArray = argumentRead.obj instanceof Array;
				if (change.start == item.start){
					// prepend
					injectItem(beginningOfLineByteOffset, beginningOfLineByteOffset, newline);
					if (change.end != item.start) {
						// delete beginning of string
						tag = tags[change.end - item.start];
						if (isArray){
							adjustment = 2;
						} else {
							adjustment = 1;
						}

						decodedStreamChanges.push({
							start: argumentRead.streamBeginPos + adjustment, // ( or <
							end: tag.taggedString.streamBeginPos + 1 + tag.taggedStringIndex + (tag.length || 0),
							bytes: []
						});
					}
					if (newline) {
						decodedStreamChanges.push({
							start: commandRead.streamBeginPos,
							end: commandRead.streamEndPos,
							bytes: asciiToBytes("Tj")
						});
					}
					return;
				} else if (change.end == item.end){
					// append
					if (change.start != item.end) {
						// delete end of string
						tag = tags[change.start - item.start];
						if (isArray){
							adjustment = 2;
						} else {
							adjustment = 1;
						}
						decodedStreamChanges.push({
							start: tag.taggedString.streamBeginPos + 1 + tag.taggedStringIndex,
							// TODO: keep the spacing argument that would've been deleted?
							end: argumentRead.streamEndPos - adjustment, // ], (>|))
							bytes: []
						});
					}
					injectItem(commandRead.streamEndPos, commandRead.streamEndPos);
					return;
				} else {
					// change is in the middle of the item.
					var startTag = tags[change.start - item.start];
					var endTag = tags[change.end - item.start];
					decodedStreamChanges.push({
						start: startTag.taggedString.streamBeginPos + 1 + startTag.taggedStringIndex,
						end: endTag.taggedString.streamBeginPos + 1 + endTag.taggedStringIndex, // ( or <
						bytes: asciiToBytes(
							(startTag.taggedString.type == 'hexstring' ? '>' : ')')
							+ (isArray ? ']' : '')
							+ ' ' + origCommand + '\n'
							+ firstFontString
							+ "\n(" + change.string + ") " + 'Tj' + "\n"
							+ secondFontString
							+ (isArray ? '[' : '')
							+ (endTag.taggedString.type == 'hexstring' ? '<' : '(')
						)
					});
					if (newline) {
						decodedStreamChanges.push({
							start: commandRead.streamBeginPos,
							end: commandRead.streamEndPos,
							bytes: asciiToBytes("Tj")
						});
					}
				}
			});
		});

		contentStream.reset();
		var dict = contentStream.dict;
		var dictReads = getReads(this.pdfDocument.xref.stream.makeSubStream(dict.streamBeginPos, dict.streamEndPos - dict.streamBeginPos));
		var dictLexerReads = dictReads.lexer;

		// pdfjs will automatically get the correct dictionary and it will be tagged with the needed offsets
		var fontDict = this.pages[page.metrics.index].page.resources.get('Font');

		if (!fontDict) {
			// TODO: need to create a resource dictionary for this page
			// or is it that it didn't give me the right dictionary?
			throw new Error("Font dictionary not found");
		}

		if (!fontDict.map.CMFONT){
			byteChanges.push({
				start: fontDict.streamBeginPos + 2,
				end: fontDict.streamBeginPos + 2,
				bytes: asciiToBytes(
					" /CMFONT << /Type /Font /Subtype /Type1 /BaseFont /Helvetica >> "
				)
			});
		}

		// with the decoded stream changes, make a new byte buffer for the stream,
		// fixup the stream's dictionary
		// make a change for the pdf buffer
		// TODO: current limitation: 1 content stream per page
		var newContentStreamBuffer = rippleEdit(contentStreamBuffer, decodedStreamChanges);

		byteChanges = byteChanges.concat(
			changeDict(dict, {'Length': numberToAsciiBytes(newContentStreamBuffer.length)}, dictLexerReads, this.tree),
			removeFromDict(dict, ['Filter', 'DecodeParams'], dictLexerReads),
			// TODO: verify adjustments to ensure stream and endstream aren't overwritten
			{
				start: (contentStream.streamBeginPos || contentStream.str.streamBeginPos) + 9, // 'stream'.length == 9
				end: (contentStream.streamEndPos || contentStream.str.streamEndPos) - 12, // 'endstream'.length == 12
				bytes: newContentStreamBuffer
			}

		);
	});

	byteChanges = byteChanges.sort(changeComparator);
	this.rippleEdit(byteChanges);

	function isMultibyte(tags){
		return tags.some(function(g){
			return g.taggedString.isMultibyte;
		});
	}

	function addChangeToPage(page, change){
		if (changesPerPage[page.index]){
			changesPerPage[page.index].changes.push(change);
		} else {
			changesPerPage[page.index] = {
				metrics: page,
				changes: [change]
			};
		}
	}
		
	function affectedItemsFilter(change, item, index){
		// only take prepend if 1st item
		if (change.start == change.end && change.start == item.start && !index) {
			return true;
		}
		if (change.start == change.end && change.start == item.end) return true; // append
		if (item.end <= change.start) return false; // before change
		if (item.start >= change.end) return false; // after change
		return true;
	}

	function changesPerItemMap(change, item, index, arr){
		var splitChange = {
			start: Math.max(item.start, change.start),
			end: Math.min(item.end, change.end)
		};
		var bytesStart = splitChange.start - change.start;
		var bytesEnd = index == arr.length - 1 ? undefined : splitChange.end - change.start
		splitChange.string = change.string.slice(bytesStart, bytesEnd);
		return splitChange;
	}

	function findObjectIndexByPos(reads, pos){
		for (var i = 0; i < reads.length; i++){
			var read = reads[i];
			if (read.streamBeginPos == pos) return i;
			if (read.streamBeginPos <= pos && pos < read.streamEndPos) return i;
		}
	}

	function itemFilterFactory(change) {
		return function(item){
			if (item.end <= change.start) return false; // item before change
			if (item.start >= change.end) return false; // item after change
			// item and change overlap or one contains the other
			return true;
		}
	}
}

function getReads(stream){
	var parserReads = [];
	var lexerReads = [];
	var hook = {
		log: function(r){
			if (r.level == 'parser') parserReads.push(r);
			else lexerReads.push(r);
		}
	}
	var _lexer = new parser.Lexer(stream);
	_lexer.hook = hook;
	var _parser = new parser.Parser(_lexer);
	_parser.hook = hook;

	while(!parser.isEOF(_parser.getObj())){}

	return {
		parser: parserReads,
		lexer: lexerReads
	};
}

function asciiToBytes(str){
	return str.split('').map(c=>c.charCodeAt(0));
}

// for xref offsets
function inplaceEdit(buffer, changes){
	if (!changes.length && changes.start && changes.bytes) {
		changes = [changes];
	}
	changes.forEach(function(change){
		var start = change.start;
		var bytes = change.bytes;

		for (var i = 0, ii = bytes.length; i<ii; i++){
			buffer[start + i] = bytes[i];
		}
	});
	return buffer;
}

function rippleEdit(oldBuffer, changes){
	var sizeDifference = changes.reduce(function(sum, change){
		return sum + (change.bytes.length - (change.end - change.start));
	}, 0);

	if (sizeDifference == 0) {
		return inplaceEdit(oldBuffer, changes);
	}

	var oldBufferLength = oldBuffer.byteLength || oldBuffer.length
	var newBuffer = new Uint8Array(oldBufferLength + sizeDifference);

	var newPos = 0, oldPos = 0;
	changes.forEach(function(change){
		var start = change.start;
		var end = change.end;
		var bytes = change.bytes;

		while(oldPos < start){
			newBuffer[newPos++] = oldBuffer[oldPos++];
		}
		for (var j = 0, jj = bytes.length; j<jj; j++){
			newBuffer[newPos++] = bytes[j];
		}
		oldPos = end;
	});

	while(oldPos < oldBufferLength){
		newBuffer[newPos++] = oldBuffer[oldPos++];
	}

	// assert newPos == newBuffer.length

	return newBuffer;
}


CMPDF.prototype.rippleEdit = function(changes){
	// TODO: this should return a new instance rather than
	// reinitializing this current instance
	// perhaps even make this a class method instead of an instance
	var tree = this.tree;
	var stream = this.pdfDocument.xref.stream;
	var oldBuffer = this.buffer;
	var oldBufferLength = oldBuffer.byteLength || oldBuffer.length;

	var AffectedRanges = {
		affectedRanges: [{
			start: 0,
			end: oldBufferLength,
			offsetAdjustment: 0
		}],
		adjustOffset: function(offset){
			return offset + this.getOffsetAdjustment(offset);
		},
		getOffsetAdjustment: function(offset){
			return this.findAffectedRange(offset).offsetAdjustment;
		},
		addChanges: function(changes, globalOffsetAdjustment){
			if (!globalOffsetAdjustment) globalOffsetAdjustment = 0;
			changes.forEach((change) => {
				var index = this.findAffectedRangeIndex(change.start - globalOffsetAdjustment);
				var range = this.affectedRanges[index];
				var newRanges = [{
					start: range.start,
					end: change.start - globalOffsetAdjustment,
					offsetAdjustment: range.offsetAdjustment
				}, {
					start: change.start - globalOffsetAdjustment,
					end: range.end,
					offsetAdjustment: range.offsetAdjustment + change.bytes.length - (change.end - change.start)
				}];
				[].splice.apply(this.affectedRanges, [index, 1].concat(newRanges));
			})
		},
		findAffectedRangeIndex: function(offset){
			for (var i = 0, ii = this.affectedRanges.length; i<ii; i++){
				var range = this.affectedRanges[i];
				if (offset >= range.start && offset < range.end)
					return i;
			}
		},
		findAffectedRange: function(offset){
			return this.affectedRanges[this.findAffectedRangeIndex(offset)];
		}
	};

	var newBuffer; // TODO: hacky
	_rippleEdit(oldBuffer, changes);

	makeDlLink(newBuffer, 'preparse.pdf');
	CMPDF.call(this, newBuffer);
	makeDlLink(newBuffer, 'postparse.pdf');

	function _rippleEdit(buffer, changes){
		var bufferLength = buffer.byteLength || buffer.length;
		var adjustedChanges = changes.map(function(change){
			return {
				start: AffectedRanges.adjustOffset(change.start),
				end: AffectedRanges.adjustOffset(change.end ? change.end : (change.start + change.bytes.length)),
				bytes: change.bytes
			};
		})
		newBuffer = rippleEdit(buffer, adjustedChanges);

		// if this.buffer == newBuffer || sizeDifference == 0, inplaceedit
		
		var sizeDifference = newBuffer.length - bufferLength;
		if (!sizeDifference)
			return newBuffer;

		// NOTE: offsets don't change when the buffer size didn't change
		// that's why we return early

		// changes has offsets relative to the original buffer
		AffectedRanges.addChanges(changes);


		// AffectedRanges.addChanges(changes, accumulatedSizeDifference);

		updateOffsets(newBuffer);
		
		return newBuffer;
	}

	function updateOffsets(buffer) {
		var xrefs = tree.xrefs.filter(function(x){
			return !x.done;
		}).sort(hookEntriesComparator);


		xrefs.forEach(function(xref){
			if (xref.done)
				return;
			xref.done = true;
			if (xref.type == 'classic'){
				updateXrefTable(xref);
				updateXrefTrailer(xref);
			} else if (xref.type == 'stream') {
				// TODO: assuming that xref stream is anywhere in the document AND
				// there will always be at least have empty xref tables
				// TODO: this isn't necessarily the case. startxref can point to a xref stream
				// but i don't know yet how to find/keep track of startxrefs
				// so that's why we're not updating the trailer in this branch
				updateXrefStream(xref);
			}
		});

		// TODO: ensure that AT LEAST the last startxref has been updated
		// by searching backwards from the end of the file
		// and finding 'startxref' (maybe using pdfjs facilities to do so)
		// and, using AffectedRanges, updating it's offset if needed

		function updateXrefStream(xref){

			var xrefStream = xref.stream;
			var xrefDict = xrefStream.dict;

			var entries = xref.entries;
			var origW = xrefDict.map.W
			var newW = [origW[0], origW[1], origW[2]];
			var max = Math.max;

			var needsEdit = false;

			var newEntries = entries.sort(function(a,b){
					return a.num - b.num;
				}) // should already be sorted. do we need this?
				.map(function(entry){
					var offset = entry.offset;
					var type = entry.type;
					if (type == 1) {
						var offsetAdjustment = AffectedRanges.getOffsetAdjustment(offset);
						if (offsetAdjustment){
							needsEdit = true;
							offset = offset + offsetAdjustment;
						}
					}
					return {
						type: type,
						offset: offset,
						gen: entry.gen
					}
				});

			if (!needsEdit) {
				return;
			}

			newEntries.forEach(function(entry){
				newW[0] = max(newW[0], numBytes(entry.type));
				newW[1] = max(newW[1], numBytes(entry.offset));
				newW[2] = max(newW[2], numBytes(entry.gen));
			});

			// var bytesPerEntry = newW[0] + newW[1] + newW[2];


			var newXrefBytes = newEntries.map(function(entry){
				return [].concat(
					numberToBytesBE(entry.type, newW[0]),
					numberToBytesBE(entry.offset, newW[1]),
					numberToBytesBE(entry.gen, newW[2])
				);
			}).reduce(function(bytes, entryBytes){
				return bytes.concat(entryBytes);
			}, []);

			// assert newXrefBytes.length == bytesPerEntry * newEnties.length


			var wChanged = false;
			for (var i = 0; i < 3; i++){
				wChanged = newW[i] != origW[i];
			}

			var newWBytes;
			if (wChanged) {
				newWBytes = [].concat(
					'['.charCodeAt(0),
					newW.join(' ').split('').map(x=>x.charCodeAt(0)),
					']'.charCodeAt(0)
				);
			}

			// TODO: put into a function
			var lexerReads = [];
			var parserReads = [];
			var hook = {
				log: function(r){
					if (r.level == 'parser') parserReads.push(r);
					else lexerReads.push(r);
				}
			}
			var _lexer = new parser.Lexer(stream.makeSubStream(xrefDict.streamBeginPos, xrefDict.streamEndPos - xrefDict.streamBeginPos));
			_lexer.hook = hook;
			var _parser = new parser.Parser(_lexer);
			while(!parser.isEOF(_parser.getObj())){
				;
			}

			var changes = [];

			// TODO: this would be easier with a dictToBytes() function

			var toRemove = [];
			if (xrefDict.map.Filter)
				toRemove.push('Filter');
			if (xrefDict.map.DecodeParams)
				toRemove.push('DecodeParams');
			changes = changes.concat(removeFromDict(xrefDict, toRemove, lexerReads));

			if (newWBytes){
				changes = changes.concat(changeDict(xrefDict, {'W': newWBytes}, lexerReads));
			}

			changes.push({
				start: xref.streamBeginPos,
				end: xref.streamEndPos,
				bytes: newXrefBytes
			})

			changes = changes.sort(changeComparator);

			_rippleEdit(buffer, changes);

			function numBytes(num){
				var multiplier = 256; // 2^8
				var bytes = 0;
				var max = 1;
				while(max <= num){
					bytes++;
					max = max * multiplier;
				}
				return bytes;
			}

			function numberToBytesBE(num, fixedWidth){
				if (fixedWidth && num >= Math.pow(2, 8*fixedWidth)) {
					throw new Error("Number too large to fit into fixed number of bytes wanted");
				}
				var bytes = [];

				for (var i = 0; num != 0; i++){
					// NOTE: converts num to 32bit integer
					// take the lowest (least significant byte)
					// prepend to bytes array
					// shift num
					bytes.unshift(num & 255)
					num = num >>> 8;
				}

				if (!fixedWidth)
					return bytes;

				var prepend = new Array(fixedWidth - bytes.length);
				prepend.fill(0);
				return prepend.concat(bytes);
			}

			function numberToBytesBE2(num, fixedWidth){
				var byteLength = numBytes(num);
				if (byteLength > fixedWidth)
					throw new Error("Number too large to fit into fixed number of bytes wanted");
				var u8a = new Uint8Array(max(byteLength, 4));
				var dv = new DataView(u8a.buffer);
				dv.setUint32(0, num); // big-endian

				var bytes;
				if (byteLength < u8a.length){
					bytes = [].slice.call(u8a, 4 - byteLength);

				} else {
					bytes = [].slice.call(u8a);
				}

				if (!fixedWidth)
					return bytes;

				var prepend = new Array(fixedWidth - bytes.length);
				prepend.fill(0)
				return prepend.concat(bytes);
			}
		}

		function updateXrefTrailer(xref){
			// assume streamEndPos marks beginning of `trailer`
			
			var reads = [];
			var parserReads = [];
			var hook = {
				log: function(r){
					if (r.level == 'parser') parserReads.push(r);
					else reads.push(r);
				}
			};
			var _stream = stream.makeSubStream(xref.streamEndPos);
			var lexer = new parser.Lexer(_stream);
			lexer.hook = hook;

			var _parser = new parser.Parser(lexer);
			_parser.hook = hook;

			var trailerDict, startXrefOffset;
			var obj;
			obj = _parser.getObj(); // trailer or startref
			if (prim.isCmd(obj, 'trailer')){
				trailerDict = _parser.getObj();
				_parser.getObj(); // startxref
				startXrefOffset = _parser.getObj();
			} else if (prim.isCmd(obj, 'startxref')){
				startXrefOffset = _parser.getObj();
			} else {
				// uh oh...
				throw new Error("didn't find trailer at end of xref");
			}

			var prev = trailerDict.get("Prev");
			var xrefStm = trailerDict.get("XRefStm");

			var edits = [];

			var i, ii, read;
			var prevAdjustment = prev ? AffectedRanges.getOffsetAdjustment(prev) : 0;
			var xrefStmAdjustment = xrefStm ? AffectedRanges.getOffsetAdjustment(xrefStm) : 0;
			var startXrefAdjustment = startXrefOffset ? AffectedRanges.getOffsetAdjustment(startXrefOffset) : 0;
			if (prevAdjustment){
				for (i = 0, ii = reads.length; i<ii; i++){
					if (prim.isName(reads[i]) && reads[i].obj.name == 'Prev'){
						break;
					}
				}
				read = reads[i+1];
				edits.push({
					start: read.streamBeginPos,
					end: read.streamEndPos,
					bytes: numberToAsciiBytes(prev + prevAdjustment)
				});
			}
			if (xrefStmAdjustment){
				for (i = 0, ii = reads.length; i<ii; i++){
					if (prim.isName(reads[i]) && reads[i].obj.name == 'XRefStm'){
						break;
					}
				}
				read = reads[i+1];
				edits.push({
					start: read.streamBeginPos,
					end: read.streamEndPos,
					bytes: numberToAsciiBytes(xrefStm + xrefStmAdjustment)
				});
			}
			if (startXrefAdjustment){
				for (i = 0, ii = reads.length; i<ii; i++){
					if (prim.isCmd(reads[i].obj, 'startxref')){
						break;
					}
				}
				read = reads[i+1];
				edits.push({
					start: read.streamBeginPos,
					end: read.streamEndPos,
					bytes: numberToAsciiBytes(startXrefOffset + startXrefAdjustment)
				});
			}

			if (!edits.length) {
				return;
			}

			edits = edits.sort(changeComparator);

			return _rippleEdit(buffer, edits);
		}

		function updateXrefTable(xref){

			var _objects = getObjects(stream, xref.streamBeginPos, xref.streamEndPos, {cmd: 'trailer'});
			var objects = _objects.objects;
			var reads = _objects.reads;
			var changes = [];

			// i == 1: skip xref command
			// ii == objects.length - 1: last object is trailer
			var i = 1, ii = objects.length - 1;
			var offset, type, offsetReadIndex;
			while(i < ii){
				offset = objects[i++];
				offsetReadIndex = i - 1;
				i++; // gen
				type = objects[i++];

				if (!(type instanceof prim.Cmd)) {
					// section heading
					offset = type;
					offsetReadIndex = i - 1;
					i++; // gen
					type = objects[i++];
				}

				if (!type) {
					// we must've reached EOF?
					break;
				}

				if (type.cmd == 'n') {
					var offsetAdjustment = AffectedRanges.getOffsetAdjustment(offset);
					if (offsetAdjustment){
						changes.push({
							start: reads[offsetReadIndex].streamBeginPos,
							bytes: numberToAsciiBytes(offset + offsetAdjustment, 10)
						})

					}
				}
			}

			if (changes.length){
				_rippleEdit(buffer, changes.sort(changeComparator));
			}
		}
	}
}

// TODO: removeFromDict and changeDict are so similar they should share an implementation
function removeFromDict(dict, names, lexerReads){
	var changes = [];
	var i,ii;
	var obj;
	var read;
	var nameIndex;
	var removeFromDict = names.slice();
	for (i = 0, ii = lexerReads.length; (i<ii) && removeFromDict.length; i++){
		read = lexerReads[i];
		nameIndex = removeFromDict.indexOf(read.obj.name);
		if (prim.isName(read.obj) && nameIndex > -1){
			removeFromDict.splice(nameIndex, 1);
			obj = lexerReads[i+1];
			changes.push({
				start: read.streamBeginPos,
				end: read.streamEndPos,
				bytes: []
			}, {
				start: obj.streamBeginPos,
				end: obj.streamEndPos,
				bytes: []
			});
		}
	}

	return changes;
}

function changeDict(dict, dictChanges, lexerReads, tree){
	var changes = [];
	var i,ii;
	var obj;
	var read;
	var namesLeft = Object.keys(dictChanges).length;
	for (i = 0, ii = lexerReads.length; (i<ii) && namesLeft; i++){
		read = lexerReads[i];
		if (prim.isName(read.obj) && read.obj.name in dictChanges){
			if (prim.isRef(dict.map[read.obj.name])){
				var ref = dict.map[read.obj.name];
				var entry = tree.entries[ref.num][ref.gen];
				changes.push({
					start: entry.streamBeginPos + (ref.num + ' ' + ref.gen + ' obj\n').length,
					end: entry.streamEndPos - 1,
					bytes: dictChanges[read.obj.name]
				});
			} else {
				obj = lexerReads[i+1];
				changes.push({
					start: obj.streamBeginPos,
					end: obj.streamEndPos,
					bytes: dictChanges[read.obj.name]
				});
			}
			namesLeft--;
		}
	}

	return changes;
}

function numberToAsciiBytes(number, fixedWidth, radix) {
	var str = number.toString(radix || 10).split('');
	if (fixedWidth) {
		var padding = new Array((fixedWidth || 10) - str.length);
		padding.fill('0');
		str = padding.concat(str);
		if (str.length != fixedWidth){
			throw new Error("Length of number greater than fixed width");
		}
	}
	str = str.map(function(d){return d.charCodeAt(0)});
	return str;
}

function getObjects(stream, start, end, until){
	var objects = [];
	var reads = [];
	var hook = {
		log: function(r,l){
			if (l) r.level = l;
			reads.push(r);
		}
	};
	stream = stream.makeSubStream(start, end - start);
	var lexer = new parser.Lexer(stream);
	lexer.hook = hook;
	var obj;

	var _until;
	if (
		typeof until == 'string' || until instanceof String
		|| typeof until == 'number' || until instanceof Number
		) {
		_until = function(o){return o == until};
	} else if (!until){
		_until = parser.isEOF;
	} else if (typeof until == 'object'){
		if (until.cmd) {
			_until = function(o){return o && o.cmd && o.cmd == until.cmd};
		} else if (until.name) {
			_until = function(o){return o && o.name && o.name == until.name};
		}
	} else if (typeof until == 'function'){
		_until = until;
	}

	while (!_until(obj) && !parser.isEOF(obj)){
		objects.push(obj = lexer.getObj());
	}

	return {objects: objects, reads: reads};
}

function buildTree(reads, xrefs){
	var tree = [];
	var entries = {};
	var kids = [];
	var parents = [];

	reads.forEach(function(currentRead){
		var xrefEntry = currentRead.xrefEntry;
		if (!xrefEntry){
			tree.push(currentRead);
			return; // xref table
		}
		if (!entries[xrefEntry.num])
			entries[xrefEntry.num] = {};
		if (entries[xrefEntry.num][xrefEntry.gen])
			return; // duplicate
		entries[xrefEntry.num][xrefEntry.gen] = currentRead;
		currentRead.xref = matchEntryToXref(xrefEntry)
		if (currentRead.parentNumber) {
			kids.push(currentRead);
			return;
		}
		tree.push(currentRead);
	});

	function matchEntryToXref(xrefEntry){
		for (var i = 0, ii = xrefs.length; i < ii; i++){
			var xref = xrefs[i];

			var entry = xref.entries[xrefEntry.num];
			if (entry && entry.gen == xrefEntry.gen)
				return xref;
		}
		console.error("Could not find xref for this object");
		return null;
	}

	tree = tree.sort(hookEntriesComparator);

	xrefs = xrefs.sort(hookEntriesComparator);

	kids.forEach(function(kid){
		var parent = entries[kid.parentNumber];
		if (!parent.children)
			parent.children = [];
		parent.children.push(kid);
	});

	parents.forEach(function(p){
		p.children = p.children.sort(function(a,b){
			return a.indexInParent - b.indexInParent;
		});
	});

	return {
		tree: tree,
		entries: entries,
		xrefs: xrefs
	};
}


function sanityCheck(tree, stream){
	tree.forEach(function(curr, i, arr){
		if (i == 0) return; // skip
		var prev = arr[i - 1];
		if (equalPosition(prev, curr)) return; // skip
		if (prev.streamBeginPos > curr.streamBeginPos)
			console.error("Unordered", i, prev, curr, arr);
		if (prev.streamEndPos > curr.streamBeginPos)
			console.error(new Error(`Overlap: ${prev.streamEndPos}, ${curr.streamBeginPos}`).stack, i, prev, curr, arr);
		if (prev.streamEndPos != curr.streamBeginPos)
			checkGapIsSpace(prev.streamEndPos, curr.streamBeginPos, stream) || console.error(new Error(`Gap: ${prev.streamEndPos} - ${curr.streamBeginPos}`).stack, i, prev, curr, arr);
		if (curr.children) {
			// children should be ordered
			curr.children.forEach(function(child, childI, childArr){
				if (childI == 0) return; // skip
				var prev = childArr[childI - 1];
				if (prev.indexInParent >= curr.indexInParent)
					console.error("Unordered children", curr, i, arr, child, childI, childArr);
			});
		}
	});
}


function equalPosition(a,b){
	return a.streamBeginPos == b.streamBeginPos && a.streamEndPos == b.streamEndPos;
}

function checkGapIsSpace(begin, end, stream){
	return [].slice.call(stream.getBytes(), begin, end)
	.map(c => String.fromCharCode(c))
	.join('').trim().length == 0;
}

function changeComparator(a,b){
	if (a.start == b.start)
		return a.end - b.end;
	return a.start - b.start;
}
function hookEntriesComparator(a,b){ return a.streamBeginPos - b.streamBeginPos; }

// var pdfBuf = require("fs").readFileSync(__dirname  + "/pdfs/invoice_6.pdf");
// // var pdfBuf = require("fs").readFileSync(__dirname  + "/pdfs/Test.pdf");
// var p = exports.parse(pdfBuf);
// global.p = p;

// p.getTextContent(function(err, res){
// 	p.replace([{start: 116, end: 120, string: 'Rr'}]);
// 	// p.replace([{start: 11, end: 11, string: 'ex'}, {start: 11, end: 15, string: 'That'}, {start: 60, end: 61, string: 'ex'}])
// 	// p.getTextContent(function(){
// 	// 	p.replace([{start: 61, end: 62, string: 'ex'}])
// 	// })
// });

function makeDlLink(buffer, filename){
    var blob = new Blob([buffer]);
    var a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = filename;
    a.textContent = filename;
    document.body.appendChild(a); 
}
