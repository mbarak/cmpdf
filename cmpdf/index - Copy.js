/* eslint-env browser, commonjs */
"use strict";

// var pdfBuf = require("fs").readFileSync("./libreoffice_helloworld.pdf");
var pdfBuf = require("fs").readFileSync("./invoice_6.pdf");
var pdfManagerM = require("../src/core/pdf_manager.js");
var pdfJSGlobal = require("../src/shared/global.js");
pdfJSGlobal.PDFJS.verbosity = -10;
var parser = require("../src/core/parser.js")

function getObjHook(fn_getObj, level){
	return function(){
		var stream = this.stream || this.lexer.stream;
		var streamBeginPos = stream.pos;
		var obj = fn_getObj.apply(this, arguments);
		var objInfo = {
			level: level,
			streamBeginPos: streamBeginPos - 1,
			streamEndPos: stream.pos - 1,
			obj: obj,
			stream: stream
		}
		hook.log(objInfo, level);
		return obj;
	}
}

var hook = {
	reads: [],
	xrefs: [],
	log: function(obj, level){
		this.reads.push(obj);
	},
	logXref: function(obj){
		this.xrefs.push(obj);
		this.log(obj, 'xref');
	},
	parser: function(parserConstructor){
		var hook = this;
		function CM_Parser(){
			parserConstructor.apply(this, arguments);

			this.getObj = getObjHook(this.getObj, 'parser');
		}

		CM_Parser.prototype = Object.create(parserConstructor.prototype);

		return CM_Parser;
	},

	lexer: function(lexerConstructor){
		var hook = this;
		function CM_Lexer(){
			lexerConstructor.apply(this, arguments);

			var Lexer_getObj = this.getObj;

			this.getObj = getObjHook(this.getObj, 'lexer');
		}

		CM_Lexer.prototype = Object.create(lexerConstructor.prototype);

		return CM_Lexer;
	}
};
console.log(hook);
var pdfManager = new pdfManagerM.LocalPdfManager("docId", pdfBuf, null, hook);
global.pdfManager = pdfManager;
var pdfDocument = pdfManager.pdfDocument;
global.pdfDocument = pdfDocument;

pdfDocument.parseStartXRef();
pdfDocument.parse();

var prim = require("../src/core/primitives.js")
pdfDocument.xref.entries.forEach((e, i) => pdfDocument.xref.fetch(new prim.Ref(i, e.gen)))
var tree = buildTree(hook.reads, hook.xrefs);
global.tree = tree;
// sanityCheck(tree.tree);
console.log(tree);
// var streams = findStreams(tree.tree);

var workerMod = require("../src/core/worker");

var pages = [];

for (var i = 0, ii = pdfDocument.numPages; i < ii; i++)
// var i = 10;
{
	(function(i){ // why? so i is closed-over / stable and doesn't take the last value
	pdfDocument.getPage(i)
	.then(function(page){
		return Promise.all([
			page.extractTextContent(new workerMod.WorkerTask('extractTextContent'+i), true, true),
			page.content,
			page]);
	})
	.then(function(results){
		var page = {
			index: i,
			page: results[2],
			textContent: results[0]
		};
		var contentStreams = results[1];
		if (!(contentStreams instanceof Array))
			contentStreams = [contentStreams];
		page.contentStreams = contentStreams;
		return page;
	})
	.then(function(page){
		var contentStreams = page.contentStreams;
		contentStreams.forEach(function(contentStream, j){
			var blob = new Blob([contentStream.getBytes()]);
			var a = document.createElement('a');
			var blobUrl = URL.createObjectURL(blob);
			a.href = blobUrl;
			a.download = 'download' + i + '' + j;
			a.textContent = 'download' + i + '' + j;
			document.body.appendChild(a);
			var entry;
			var objId = /(\d+)R(\d+)?/.exec(contentStream.dict.objId);
			if (objId) {
				objId = objId.slice(1);
				entry = tree.entries[objId[0]][objId[1] || 0];
			}
			else throw new Error("couldn't find this stream in tree");

			var lexer = new parser.Lexer(contentStream);
			lexer.hook = {
				reads: [],
				strings: [],
				log: function(entry){
					this.reads.push(entry);
					if (entry.type.indexOf('string') >= 0)
						this.strings.push(entry);
				}
			};
			var token;
			while(token != parser.EOF){
				try {
					token = lexer.getObj();
				} catch(e){
					// console.error(e);
					lexer.nextChar();
				}
			}
			sanityCheck(lexer.hook.reads, contentStream);
			entry.children = lexer.hook.reads;
			entry.strings = lexer.hook.strings;
			contentStream.entry = entry;
		})
		return page;
	})
	.then(page => pages[i] = page)
	.then(page => console.log(page))
	.catch(function(ex){
		console.error(ex);
		throw ex;
	})
	})(i);

}

// findStringsInStreams(streams);

function findStringsInStreams(streams){
	var OP_MAP = undefined; //require("../src/core/evaluator").OP_MAP;
	streams.forEach(function findStringInStream(stream){
		var _stream = stream;
		stream = _stream.obj;
		stream.strings = [];

		//console.log(arguments);

		try {
			var lexer = new parser.Lexer(stream, OP_MAP);
			var _parser = new parser.Parser(lexer, false, stream.dict.xref);
		} catch(e) {
			return; // not a stream that the lexer can understand
		}
		var token;
		while (token != parser.EOF){
			try {
				token = _parser.getObj();
				if (typeof token == 'string'){
					stream.strings.push(token);
				}
			} catch(e){
				lexer.nextChar();//console.log(e);
			}
		}
	});
}

function findStreams(tree){
	var streams = [];
	tree.forEach(function(node){
		if (prim.isStream(node.obj)){
			streams.push(node);
		}
		if (node.children){
			streams = streams.concat(findStreams(node.children));
		}
	});
	return streams;
}

function sanityCheck(tree, stream){
	tree.forEach(function(curr, i, arr){
		if (i == 0) return; // skip
		var prev = arr[i - 1];
		if (equalPosition(prev, curr)) return; // skip
		if (prev.streamBeginPos > curr.streamBeginPos)
			console.error("Unordered", i, prev, curr, arr);
		if (prev.streamEndPos > curr.streamBeginPos)
			console.error(new Error(`Overlap: ${prev.streamEndPos}, ${curr.streamBeginPos}`).stack, i, prev, curr, arr);
		if (prev.streamEndPos != curr.streamBeginPos)
			checkGapIsSpace(prev.streamEndPos, curr.streamBeginPos, stream) || console.error(new Error(`Gap: ${prev.streamEndPos} - ${curr.streamBeginPos}`).stack, i, prev, curr, arr);
		if (curr.children) {
			// children should be ordered
			curr.children.forEach(function(child, childI, childArr){
				if (childI == 0) return; // skip
				var prev = childArr[childI - 1];
				if (prev.indexInParent >= curr.indexInParent)
					console.error("Unordered children", curr, i, arr, child, childI, childArr);
			});
		}
	});
}

function equalPosition(a,b){
	return a.streamBeginPos == b.streamBeginPos && a.streamEndPos == b.streamEndPos;
}

function checkGapIsSpace(begin, end, stream){
	stream = stream || hook.pdfManager.pdfDocument.stream;
	return [].slice.call(stream.getBytes(), begin, end)
	.map(c => String.fromCharCode(c))
	.join('').trim().length == 0;
}

function buildTree(reads, xrefs){
	var tree = [];
	var entries = {};
	var kids = [];
	var parents = [];

	reads.forEach(function(currentRead){
		var xrefEntry = currentRead.xrefEntry;
		if (!xrefEntry){
			tree.push(currentRead);
			return; // xref table
		}
		if (!entries[xrefEntry.num])
			entries[xrefEntry.num] = {};
		if (entries[xrefEntry.num][xrefEntry.gen])
			return; // duplicate
		entries[xrefEntry.num][xrefEntry.gen] = currentRead;
		currentRead.xref = matchEntryToXref(xrefEntry)
		if (currentRead.parentNumber) {
			kids.push(currentRead);
			return;
		}
		tree.push(currentRead);
	});

	function matchEntryToXref(xrefEntry){
		for (var i = 0, ii = xrefs.length; i < ii; i++){
			var xref = xrefs[i];

			if (xref.entries[xrefEntry.num] && xref.entries[xrefEntry.num][xrefEntry.gen])
				return xref;
		}
		console.error("Could not find xref for this object");
		return null;
	}

	tree = tree.sort((a,b) => a.streamBeginPos - b.streamBeginPos);

	kids.forEach(function(kid){
		var parent = entries[kid.parentNumber][0];
		if (!parent.children)
			parent.children = [];
		parent.children.push(kid);
	});

	parents.forEach(function(p){
		p.children = p.children.sort(function(a,b){
			return a.indexInParent - b.indexInParent;
		});
	});

	return {
		tree: tree,
		entries: entries
	};

	var sortedReads = reads.sort(sortWithOuterFirst);

	function sortWithOuterFirst(a,b) {
		if (a.streamBeginPos === b.streamBeginPos) {
			// outermost obj is first. This is important. the code below is using this assumption
			return b.streamEndPos - a.streamEndPos;
		}
		return a.streamBeginPos - b.streamBeginPos;
	}

	var uniqueReads = sortedReads.filter(function(el,i, arr) {
		if (i == 0) return true;
		if (equalReads(el, arr[i-1])) return false;
		return true;
	});

	uniqueReads.forEach(function(read){
		if (read.parentNumber) {
			kids.push(read);
			return;
		}

		if (read.xrefEntry && read.xrefEntry.gen == 0) {
			entries[read.xrefEntry.num] = read;
		}
		tree.push(read);
	});

	kids.forEach(function(kid){
		var entry = entries[kid.parentNumber];
		if (!entry.children) {
			entry.children = [];
			parents.push(entry);
		}
		entry.children.push(kid);
	});

	parents
		.forEach(function(entry){
			entry.children = entry.children.sort(function(a,b){
				return a.indexInParent - b.indexInParent;
			});
		});

	return tree;

	function placeInTree(tree, read){
		var search = findInSorted(tree, read, searchByPosition);
		if (search.found) {
			if (equalPosition(read, search.found)){
				insert();
			}
			// read found in tree contains current read
			else if (search.found.children){
				placeInTree(search.found.children, read)
				search.found.children.sort(sortWithOuterFirst);
			} else {
				search.found.children = [read];
			}
		} else {
			insert();
		}

		function insert(){
			tree.splice(search.index, 0, read);
		}
	}

	function searchByPosition(a,b){
		if (a.streamBeginPos >= b.streamBeginPos && a.streamEndPos <= b.streamEndPos)
			return 0; // a is equal to or contained by b
		else if (b.streamBeginPos >= a.streamBeginPos && b.streamEndPos <= a.streamEndPos)
			return 0; // b is equal to or contained by a
		else if (a.streamEndPos <= b.streamBeginPos)
			return -1; // a before b
		else if (a.streamBeginPos >= b.streamEndPos)
			return 1; // a after b
		else {// a and b are overlapping?
			console.error(new Error("Invalid reads encountered").stack);
			return a.streamBeginPos - b.streamBeginPos;
		}

	}

	function equalPosition(a,b){
		return a.streamBeginPos == b.streamBeginPos && a.streamEndPos == b.streamEndPos;
	}

	function equalReads(a, b) {
		return a.level == b.level
			&& a.streamBeginPos == b.streamBeginPos
			&& a.streamEndPos == b.streamEndPos;
	}

	function findInSorted(array, needle, comparator){
		for (var i = 0, ii = array.length; i < ii; i++){
			var current = array[i];
			var currCompRes = comparator(current, needle)
			if (currCompRes < 0){
				continue;
			} else {
				return {
					index: i,
					found: currCompRes == 0 ? current : false // found : notFound
				};
			}
		}
		return {
			index: array.length,
			found: false
		};
	}
}
